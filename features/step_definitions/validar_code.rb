Quando("faço uma requisicao GET para o serviço") do                                
    @requisicao = HTTParty.get("http://jsonplaceholder.typicode.com/todos")   
  end                                                                                
                                                                                     
  Então("o código de resposta GET deve ser {string}") do |code|                          
    expect(@requisicao.response.code).to eql code
  end                                                                                
                                                                                     
  Então("imprimo ID e TITTLE de todos os itens com status code completed = true") do 
    @requisicao.each do |itens|
        if itens['completed'].eql?(true)
            if itens['id'] < 100
                puts "ID: #{itens['id']} ...... Tittle: #{itens['title']}"
            else
                puts "ID: #{itens['id']} ..... Tittle: #{itens['title']}"
            end
        end
    end
  end
