require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
require 'rspec'
require 'httparty'

Capybara.configure do |config|
    config.default_driver = :selenium_chrome
end

Capybara.default_max_wait_time = 15
