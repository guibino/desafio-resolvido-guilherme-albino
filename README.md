# Instruções para execução:

Após ter o todo o ambiente preparado, executar os comandos:

* bundle install
* cucumber => Para  rodar todo o projeto desta pasta
* cucumber -t <TAG> => Para rodar apenas a parte do projeto referente à <TAG>:
* <TAGS>
* "@ + novo_supplier" => Para fazer o cadastro de um novo supplier
* "@ + post" => Para criar uma atividade
* "@ + get" => Para validação e exibição dados